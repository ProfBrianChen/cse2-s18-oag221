// Olivia Grimes, 2/16/18, cse02



public class CardGenerator{
  // main method required for every Java program
  public static void main (String[] args) {
    
    int number = (int)(Math.random()*52) + 1;
    
    String suit = "abc";
    String identity = "def";
    
    if ( number < 14 ){
      suit = "Diamonds";
    }
    else if ( number < 27 ){
      suit = "Clubs";
    }
    else if (number < 40 ){
      suit = "Hearts";
    }
    else if ( number < 53 ){
      suit = "Spades";
    }
    
    switch (number){
      case 1:
      case 14:
      case 27:
      case 40:
        identity = "Ace";
        break;
      case 2:
      case 15:
      case 28:
      case 41:
        identity = "2";
        break;
      case 3:
      case 16:
      case 29:
      case 42:
        identity = "3";
        break;
      case 4:
      case 17:
      case 30:
      case 43:
        identity = "4";
        break;
      case 5:
      case 18:
      case 31:
      case 44:
        identity = "5";
        break;
      case 6:
      case 19:
      case 32:
      case 45:
        identity = "6";
        break;
      case 7:
      case 20:
      case 33:
      case 46:
        identity = "7";
        break;
      case 8:
      case 21:
      case 34:
      case 47:
        identity = "8";
        break;
      case 9:
      case 22:
      case 35:
      case 48:
        identity = "9";
        break;
      case 10:
      case 23:
      case 36:
      case 49:
        identity = "10";
        break;
      case 11:
      case 24:
      case 37:
      case 50:
        identity = "Jack";
        break;
      case 12:
      case 25:
      case 38:
      case 51:
        identity = "Queen";
        break;
      case 13:
      case 26:
      case 39:
      case 52:
        identity = "King";
        break;
        
    }

    System.out.println("The random number is "+ number);
    System.out.println("You picked the "+ identity +" of "+ suit);
    
    
  }
}