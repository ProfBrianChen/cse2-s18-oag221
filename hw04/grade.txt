Grade:  100/100
Comments:
A) Does the code compile?  How can any compiler errors be resolved?
program compiles correctly.
B) If the code compiles, does the code run properly?  What kinds of input cause a runtime error?
It runs without error.
C) How can any runtime errors be resolved?
N/A
D) What topics should the student study in order to avoid the errors they made in this homework?
N/A
E) Other comments:
Good use of comments on this. 