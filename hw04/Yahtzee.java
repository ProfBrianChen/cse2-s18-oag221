// Olivia Grimes, cse02, 2/19/18

import java.util.Scanner;

public class Yahtzee{
  // main class
  public static void main (String[] args) {
    
    int dice1 = 0;
    int dice2 = 0;
    int dice3 = 0;
    int dice4 = 0;
    int dice5 = 0;
    
    Scanner myScanner = new Scanner( System.in ); // scanner to input values
    
    System.out.println("Would you like to manually enter dice values? If yes type 1. If no type 2 ");
    int chooseNumbers = myScanner.nextInt(); // choose whether to randomly generate or manually input dice values
    
    
    if ( chooseNumbers < 1 || chooseNumbers > 2){
      System.out.println("Error");
      System.exit(0); // creating error for incorrect input
    }
    else if ( chooseNumbers == 1 ){
      System.out.println("Enter value for first dice ");
      dice1 = myScanner.nextInt();
      System.out.println("Enter value for second dice ");
      dice2 = myScanner.nextInt();
      System.out.println("Enter value for third dice ");
      dice3 = myScanner.nextInt();
      System.out.println("Enter value for forth dice ");
      dice4 = myScanner.nextInt();
      System.out.println("Enter value for fifth dice ");
      dice5 = myScanner.nextInt();
      System.out.println(dice1 +" "+ dice2 +" "+ dice3 +" "+ dice4 +" "+ dice5);
      if (dice1 < 1 || dice1 > 6 || dice2 < 1 || dice2 > 6 || dice3 < 1 || dice3 > 6 || dice4 < 1 || dice4 > 6 || dice5 < 1 || dice5 > 6){
        System.out.println("Error. Values must be between 1 and 6.");
        System.exit(0);
      }
    } // allows user to input dice values
  
    else if (chooseNumbers == 2) {
    dice1 = (int)(Math.random()*6) + 1;
    dice2 = (int)(Math.random()*6) + 1;
    dice3 = (int)(Math.random()*6) + 1;
    dice4 = (int)(Math.random()*6) + 1;
    dice5 = (int)(Math.random()*6) + 1;
    System.out.println("Your randomly generated dice values are "+dice1 +" "+ dice2 +" "+ dice3 +" "+ dice4 +" "+ dice5);
    } // allows user to randomly generate dice values
    
   
   
    // counting number of aces, twos, threes, etc. that the five dices show
    int countAce = 0;
    int countTwo = 0;
    int countThree = 0;
    int countFour = 0;
    int countFive = 0;
    int countSix = 0;
    
    
    //// Upper Section
    
    // counting aces
    if (dice1==1){
      countAce++;
    }
    if (dice2==1){
      countAce++;
    }
    if (dice3==1){
      countAce++;
    }
    if (dice4==1){
      countAce++;
    }
    if (dice5==1){
      countAce++;
    }
    
    int scoreAce = countAce;
    System.out.println("Your score for aces is "+ scoreAce); // print score of aces
    
    // counting twos
    if (dice1==2){
      countTwo++;
    }
    if (dice2==2){
      countTwo++;
    }
    if (dice3==2){
      countTwo++;
    }
    if (dice4==2){
      countTwo++;
    }
    if (dice5==2){
      countTwo++;
    }
    int scoreTwo = 2 * countTwo;
    System.out.println("Your score for twos is "+ scoreTwo); // print score of twos
    
    // counting threes
    if (dice1==3){
      countThree++;
    }
    if (dice2==3){
      countThree++;
    }
    if (dice3==3){
      countThree++;
    }
    if (dice4==3){
      countThree++;
    }
    if (dice5==3){
      countThree++;
    }
    int scoreThree = 3 * countThree;
    System.out.println("Your score for threes is "+ scoreThree); // print score of threes
    
    // counting fours
    if (dice1==4){
      countFour++;
    }
    if (dice2==4){
      countFour++;
    }
    if (dice3==4){
      countFour++;
    }
    if (dice4==4){
      countFour++;
    }
    if (dice5==4){
      countFour++;
    }
    int scoreFour = 4 * countFour;
    System.out.println("Your score for fours is "+ scoreFour); // print score of fours
    
    // counting fives
    if (dice1==5){
      countFive++;
    }
    if (dice2==5){
      countFive++;
    }
    if (dice3==5){
      countFive++;
    }
    if (dice4==5){
      countFive++;
    }
    if (dice5==5){
      countFive++;
    }
    int scoreFive = 5 * countFive;
    System.out.println("Your score for fives is "+ scoreFive);
    
    // counting sixes
    if (dice1==6){
      countSix++;
    }
    if (dice2==6){
      countSix++;
    }
    if (dice3==6){
      countSix++;
    }
    if (dice4==6){
      countSix++;
    }
    if (dice5==6){
      countSix++;
    }
    int scoreSix = 6 * countSix;
    System.out.println("Your score for sixes is "+ scoreSix); // print score of sixes
    
    int totalUpper = scoreAce + scoreTwo + scoreThree + scoreFour + scoreFive + scoreSix;
    System.out.println("Your total score for the upper section is "+ totalUpper); // print score of upper section without bonus
    
    int totalBonus;
    if (totalUpper >= 63){
      totalBonus = totalUpper + 25;
      System.out.println("Your total score including the bonus is " + totalBonus); // print score of upper section with bonus (if there is one)
    }
    
    
    
    //// Lower Section
    
    int threeOfAKind;
    int fourOfAKind;
    int lowerTot =0;
    
    if (countAce == 5 | countTwo == 5 | countThree == 5 | countFour == 5 | countFive == 5 | countSix == 5){
      System.out.println("You rolled Yahtzee!! 50 points."); // yahtzee
      lowerTot = 50;
    }
    else if ((countAce == 3 | countTwo == 3 | countThree == 3 | countFour == 3 | countFive == 3 | countSix == 3) &&
             (countAce == 2 | countTwo == 2 | countThree == 2 | countFour == 2 | countFive == 2 | countSix == 2)){
      System.out.println("You rolled a full house. 25 Points."); // full house
      lowerTot = 25;
    }
    else if (countAce == 3 || countTwo == 3 || countThree == 3 || countFour == 3 || countFive == 3 || countSix == 3){
      threeOfAKind = dice1 + dice2 + dice3 + dice4 + dice5;
      System.out.println("You rolled three of a kind. "+ threeOfAKind + " points."); // three of a kind
      lowerTot = threeOfAKind;
    }
    else if (countAce == 4 | countTwo == 4 | countThree == 4 | countFour == 4 | countFive == 4 | countSix == 4){
      fourOfAKind = dice1 + dice2 + dice3 + dice4 + dice5;
      System.out.println("You rolled four of a kind. "+ fourOfAKind + " points."); // four of a kind
      lowerTot = fourOfAKind;
    }
    else if ((countAce == 1 && countTwo == 1 && countThree == 1 && countFour == 1 && countFive == 1) ||
            (countTwo == 1 && countThree == 1 && countFour == 1 && countFive == 1 && countSix == 1)){
      System.out.println("You rolled a large straight. 40 points."); // large straight
      lowerTot = 40;
    }
    // small straight
    else if ((countAce ==2 && countTwo ==1 && countThree ==1 && countFour ==1) ||
             (countAce ==1 && countTwo ==2 && countThree ==1 && countFour ==1) ||
             (countAce ==1 && countTwo ==1 && countThree ==2 && countFour ==1) ||
             (countAce ==1 && countTwo ==1 && countThree ==1 && countFour ==2) ||
             (countAce ==1 && countTwo ==1 && countThree ==1 && countFour ==1 && countSix ==1) ||
             (countTwo ==2 && countThree ==1 && countFour ==1 && countFive ==1) ||
             (countTwo ==1 && countThree ==2 && countFour ==1 && countFive ==1) ||
             (countTwo ==1 && countThree ==1 && countFour ==2 && countFive ==1) || 
             (countTwo ==1 && countThree ==1 && countFour ==1 && countFive ==2) || 
             (countAce ==1 && countThree ==1 && countFour ==1 && countFive ==1 && countSix ==1) ||
             (countTwo ==2 && countThree ==1 && countFour ==1 && countFive ==1) ||
             (countTwo ==1 && countThree ==2 && countFour ==1 && countFive ==1) ||
             (countTwo ==1 && countThree ==1 && countFour ==2 && countFive ==1) ||
             (countTwo ==1 && countThree ==1 && countFour ==1 && countFive ==2)){
      System.out.println("You rolled a small straight. 30 points.");
      lowerTot = 30;
    }
    else {
    lowerTot = dice1 + dice2 + dice3 + dice4 + dice5;
    System.out.println("Your score for chance is "+ lowerTot); // calculate and print chance score
    }
    
    
    int finalTot = lowerTot + totalUpper;
    System.out.println("Your total score is "+ finalTot); // print total score of lower and upper sections
    
   
    
    // end of program
    
  }
}