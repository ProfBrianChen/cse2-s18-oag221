// Olivia Grimes CSE02 3/14/18

import java.util.Scanner; // import scanner

public class encrypted_x { // class
  public static void main (String[] args){ // main method

    Scanner myScanner = new Scanner(System.in); // declare a scanner

    int input = 1; // declare and initialize the variable "input"

    // checking that the value of "input" is an integer between 0 and 100, reprompting the user for another input if not
    do {System.out.println("Enter an integer between 0 and 100 ");
        while (!myScanner.hasNextInt()){
          String junkWord = myScanner.next();
          System.out.println("Enter an integer between 0 and 100 ");
        }
        input = myScanner.nextInt();
       } while (input <= 0 || input > 100);

    // creating the encrypted X
    for (int i = 0; i < input; i++){ // ranges and increments for rows
      for (int j = 0; j < input + 1; j++){ // ranges and increments for variables along the rows
        if (i == j || i == (input - j)){
          System.out.print(" "); // prints a space that will look like an X when fully printed
        }
        else {
          System.out.print("*"); // prints the stars around the X when fully printed
        }
      }
      System.out.println(); // create a new line
    }


  } // end of main method
} // end of class