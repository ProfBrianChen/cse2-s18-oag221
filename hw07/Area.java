// Olivia Grimes
// Hw 7- Area

import java.util.Scanner; // import scanner

public class Area{ // class
  
  public static void main (String[] args){ // main method
    Scanner myScanner = new Scanner(System.in);
    
    String shape = "d"; //declaring and initializing variable shape

    // allows user to enter type of shape
    do{System.out.print("Enter the word 'rectangle', 'triangle', or 'circle': ");
       while (!myScanner.hasNext()){
         String junkWord = myScanner.next();
         System.out.print("Enter the word 'rectangle', 'triangle', or 'circle': ");
       } shape = myScanner.next();
    } while (!shape.equals("rectangle") && !shape.equals("triangle") && !shape.equals("circle"));
    
    
    // PRINTING AREAS
    // finding area of rectangle
    if (shape.equals("rectangle")){
      System.out.print("Enter the height of the rectangle: ");
      while (!myScanner.hasNextDouble()){
        String junkWord = myScanner.next();
        System.out.print("Enter the height of the rectangle: ");
      } double height = myScanner.nextDouble(); // enter height of rectangle
      
      System.out.print("Enter the width of the rectangle: ");
      while (!myScanner.hasNextDouble()){
        String junkWord = myScanner.next();
        System.out.print("Enter the width of the rectangle: ");
      } double width = myScanner.nextDouble(); // enter width of rectangle
      
      double finalArea = rectArea(width, height); // call rectangle method containing formula for area of triangle using users inputs
      System.out.println("The area of the rectangle is "+ finalArea + "."); // print area of rectangle
    }
    // finding area of circle
    else if (shape.equals("circle")){
      System.out.print("Enter the radius of the circle: ");
      while (!myScanner.hasNextDouble()){
        String junkWord = myScanner.next();
        System.out.print("Enter the radius of the circle: ");
      } double radius = myScanner.nextDouble(); // enter radius of circle
      
      double finalArea = circleArea(radius); // call circle method containing formula for area of triangle using users inputs
      System.out.println("The area of the circle is "+ finalArea + "."); // print area of circle
    }
    // finding area of triangle
    else if (shape.equals("triangle")){
      System.out.print("Enter the length of the base of the triangle: ");
      while (!myScanner.hasNextDouble()){
        String junkWord = myScanner.next();
        System.out.print("Enter the length of the base of the triangle: ");
      } double base = myScanner.nextDouble(); // enter length of base of triangle
      
      System.out.print("Enter the height of the triangle: ");
      while (!myScanner.hasNextDouble()){
        String junkWord = myScanner.next();
        System.out.print("Enter the height of the triangle: ");
      } double height = myScanner.nextDouble(); // enter height of triangle
      
      double finalArea = triArea(base, height); // call triangle method containing formula for area of triangle using users inputs
      System.out.println("The area of the triangle is " + finalArea+ "."); // print the area of the triangle
    }
  }
  
  
  
  // method for area of a rectangle
  public static double rectArea(double width, double height){
    return(width * height);
  }
  
  // method for area of a triangle
  public static double triArea(double base, double height){
    return(0.5 * base * height);
  }
  
  // method for area of a circle
  public static double circleArea(double radius){
    return(Math.PI * radius * radius);
  }
  

  
  
}