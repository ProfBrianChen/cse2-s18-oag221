// Olivia Grimes
// Hw 7- String Analysis

import java.util.Scanner;

public class StringAnalysis{ // class
  // MAIN METHOD
  public static void main(String[] args){
    
    Scanner myScanner = new Scanner(System.in);
    
    String string = "abc"; // initialize and declare variable string
    int value = 1; // initialize and declare variable string
    
    // enter string to be evaluated
    System.out.print("Enter the string you would like to be evaluated: ");
    while (!myScanner.hasNext()){
      String junkWord = myScanner.next();
      System.out.print("Enter the string you would like to be evaluated: ");
    } string = myScanner.next();
    
    // allow user to choose whether to check all inputs or not
    do {System.out.print("Enter 2 if you would like to check all characters and 1 if you would only like to check some: ");
        while (!myScanner.hasNextInt()){
          String junkWord = myScanner.next();
          System.out.print("Enter 2 if you would like to check all characters and 1 if you would only like to check some: ");
        } value = myScanner.nextInt();
       } while (value > 2 || value < 1);
   
    // evaluate the string
    int size = 0;
    if (value == 1){
      // enter number of characters to evaluate
      System.out.print("Enter the number of characters you would like evaluated: ");
      while (!myScanner.hasNextInt()){
        String junkWord = myScanner.next();
        System.out.print("Enter the number of characters you would like to be evaluated: ");
      } size = myScanner.nextInt();
      
      stringMethod(string, size); // call method
    }
    else if (value == 2){
    stringMethod(string); // call method
  }
}
  
  
  
  
  // METHOD THAT ACCEPTS STRING AND INTEGERS
  public static void stringMethod(String string, int size){
    
    for (int i = 1; i <= size; i++){
      if (string.charAt(i) >= 'a' && string.charAt(i) <= 'z'){
        System.out.println(string.charAt(i) + " is a letter"); // print if it is a letter
      }
      else{
        System.out.println(string.charAt(i) + " is not a letter"); // print if it isn't a letter
      }
    }
    
    
  }
  
  
  
  
  
  // METHOD THAT ACCEPTS ONLY STRINGS
  public static void stringMethod(String string){
    int length = string.length();
    
    for (int i = 1; i < length; i++){
      if (string.charAt(i) >= 'a' && string.charAt(i) <= 'z'){
        System.out.println(string.charAt(i) + " is a letter");
        
      }
      else{
        System.out.println(string.charAt(i) + " is not a letter");
      }
    }
  }
    
  }
