// Olivia Grimes

import java.util.Scanner;
public class practice{
  public static void main(String [] arg){
    Scanner scan=new Scanner(System.in);
    int num[]=new int[10];
    int newArray1[];
    int newArray2[];
    int index,target;
    String answer="";
    
    do{
      System.out.print("Random input 10 ints [0-9]");
      num = randomInput();
      String out = "The original array is:";
      out += listArray(num);
      System.out.println(out);
 
      System.out.print("Enter the index ");
      index = scan.nextInt();
      newArray1 = delete(num,index);
      String out1 = "The output array is ";
      out1 += listArray(newArray1); //return a string of the form "{2, 3, -9}"  
      System.out.println(out1);
 
      System.out.print("Enter the target value ");
      target = scan.nextInt();
      newArray2 = remove(num,target);
      String out2 ="The output array is ";
      out2 += listArray(newArray2); //return a string of the form "{2, 3, -9}"  
      System.out.println(out2);
      System.out.print("Go again? Enter 'y' or 'Y', anything else to quit-");
      answer = scan.next();
      System.out.println();
    }while(answer.equals("Y") || answer.equals("y"));
  }
 
  public static String listArray(int num[]){
    String out = "{";
    for(int j = 0; j < num.length; j++){
      if(j > 0){
        out += ", ";
      }
      out += num[j];
    }
    out += "} ";
    return out;
  
  }

  public static int[] randomInput(){
    int[] array;
    array = new int[10];
    
    for (int i = 0; i < 10; i++){
      array[i] = (int)(Math.random() * 10);
    }
    return array;
  }
  
  
  public static int[] delete(int[] list, int pos){
    int[] array1;
    array1 = new int[list.length - 1];
    
    if (pos > 9 || pos < 0){
      System.out.println("The index is not valid");
      return list;
    }
  
    for (int k = 0; k < list.length - 1; k++){
      if (k < pos){
        array1[k] = list[k];
      }
      else if (k >= pos){
        array1[k] = list[k + 1];
      }
    }
    return array1;
  }
  
  public static int[] remove(int[] list, int target){
    int[] array2;
    
    int num = 0;
    for (int t = 0; t < list.length; t++){
      if (list[t] == target){
        num++;
      }
    }
    
    if (num == 0){
      System.out.println("Element "+ target + " was not found");
      return list;
    }
    
    System.out.println("Element " + target + " was found");
    
    array2 = new int[list.length - num];
    
    int u = 0;
    for (int s = 0; s < list.length - num; s++){
      if (list[s + u] == target){
        ++u;
        if (list[s + u] == target){
          ++u;
          if (list[s + u] == target){
            ++u;
            if (list[s + u] == target){
              ++u;
            }
          }
        }
      }
      array2[s] = list[s + u];
      
    }
    return array2;
    
  }
}