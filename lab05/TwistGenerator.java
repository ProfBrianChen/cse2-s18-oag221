// Olivia Grimes, lab06, 2/2/18, cse02
// lab05- user inputs the length of a twist, and it prints the twist of that length

import java.util.Scanner; // import scanner


public class TwistGenerator{ // class
  public static void main (String[] args){ // main method
    
    Scanner myScanner = new Scanner(System.in); // creating a scanner called myScanner
    
    int length = 1; // declaring and initializing length variable
    
    do {System.out.print("Enter a positive integer value for the length of the twist ");
      while (!myScanner.hasNextInt()){
        String junkWord = myScanner.next(); // clears scanner if value is not an integer, prompts user to enter an integer again
        System.out.print("Enter a positive integer value for the length of the twist ");
      }
        length = myScanner.nextInt();
       } while (length < 0); // if number gets here and is an integer but is negative, it will loop again
    
    
    
    int numXs = (length + 1) / 3; // formula to find the number of x's for a given length
    int LS = 0;
    int zeroSlashesTop = length / 3; // formula to find the number of slashes on top for a given length that is divisible by 3
    int A = 0;
    int oneSlashesTop = length / 3; // formula to find the number of slashes on top for a given length that has rem 1 when divided by 3
    int B = 0;
    int twoSlashesTop = length / 3; // formula to find the number of slashes on top for a given length that has rem 2 when divided by 3
    int C = 0;
    int zeroSlashesB = length / 3; // formula to find the number of slashes on bottom for a given length that is divisible by 3
    int D = 0;
    int oneSlashesB = length / 3; // formula to find the number of slashes on bottom for a given length that has rem 1 when divided by 3
    int E = 0;
    int twoSlashesB = length / 3; // formula to find the number of slashes on bottom for a given length that has rem 1 when divided by 3
    int F = 0;
    
    
    
    // making top line of slashes
    if (length % 3 == 0){
      while (zeroSlashesTop > LS){
        System.out.print("\\ /");
        ++LS;
      }
    } // top slashes if remainder of length / 3 = 0
    else if (length % 3 == 1){
      while (oneSlashesTop > B){
        System.out.print("\\ /");
        ++B;
      }
      System.out.print("\\");
    } // top slashes if remainder of length / 3 = 1
    else if (length % 3 == 2){
      while (twoSlashesTop > C){
        System.out.print("\\ /");
        ++C;
      }
      System.out.print("\\");
    } // top slashes if remainder of length / 3 = 2
    
    // creating a new line for the x's
    System.out.println("");
      
    
    
    // making x's
    while (numXs > A){
      System.out.print(" X ");
      ++A;
    } // x's printed for any given positive integer length
    
    // creating a new line for the lower slashes
    System.out.println(""); 
    
    
    
    // making lower slashes
    if (length % 3 == 0){
      while (zeroSlashesB > D){
        System.out.print("/ \\");
        ++D;
      }
    } // bottom slashes if remainder of length / 3 = 0
    else if (length % 3 == 1){
      while (oneSlashesB > E){
        System.out.print("/ \\");
        ++E;
      }
      System.out.print("/");
    } // bottom slashes if remainder of length / 3 = 1
    else if (length % 3 == 2){
      while (twoSlashesB > F){
        System.out.print("/ \\");
        ++F;
      }
      System.out.print("/");
    } // bottom slashes if remainder of length / 3 = 2
    
    // creating a new line for the next unix command
    System.out.println("");
    
  }
}