// Olivia Grimes, 2/10/18, hw03


import java.util.Scanner;

public class Pyramid{
  // main method required for every Java program
  public static void main (String[] args) {
    
    Scanner myScanner = new Scanner( System.in );
    // declaration and construction
    
    System.out.print("Enter the length of the square side of the pyramid ");
    double lengthSide = myScanner.nextDouble(); // read the value from STDIN and store in variable lengthSide
    
    System.out.print("Enter the height of the pyramid ");
    double lengthHeight = myScanner.nextDouble(); // read the value from STDIN and store in variable lengthHeight
    
    double volume = lengthSide * lengthSide * lengthHeight / 3; // calculating volume
    
    System.out.println("The volume of the pyramid is "+ volume); // output the value for volume into STDOUT
    
  }
}