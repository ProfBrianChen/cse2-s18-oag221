// Olivia Grimes, 2/10/18, hw03


import java.util.Scanner;

public class Convert{
  // main method required for every Java program
  public static void main (String[] args) {
    
    Scanner myScanner = new Scanner( System.in );
    // declaration and construction
    
    System.out.print("Enter the affected area in acres ");
    double affectedArea = myScanner.nextDouble(); // read the value from STDIN and store in variable affectedArea
    
    System.out.print("Enter the rainfall in inches in the affected area ");
    double rainfall = myScanner.nextDouble(); // read the value from STDIN and store in variable rainfall
    
    double calculate = affectedArea * 43560 * rainfall / 12 * 6.79357e-12; // calculation for converting acre inches to cubic miles
    
    System.out.println(calculate +" cubic miles are affected"); // output the value for cubic miles into STDOUT
    
  }
}