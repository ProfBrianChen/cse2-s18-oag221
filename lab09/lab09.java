// Olivia Grimes
// lab 9

public class lab09{
  public static void main(String[] args){
    
    int[] array0 = {1,2,3,4,5,6,7,8};
    System.out.print("Array0: ");
    for (int i = 0; i < 8; i++){
      System.out.print(array0[i] + " ");
    }
    System.out.println();
    
    int[] array1 = copy(array0);
    int[] array2 = copy(array0);
    
    inverter(array0);
    print(array0);
    
    
    inverter2(array1);
    print(array1);
    
    int[] array3 = inverter2(array2);
    print(array3);
    
    
    
  }
  
  public static int[] copy(int[] list){
    int[] newArray = new int[list.length]; // declare and initialize array
    
    for (int i = 0; i < list.length; i++){// for loop to copy array
      newArray[i] = list[i];
    }
    
    return newArray;
  }
  
  public static void inverter(int[] array0){
    
    for (int i = 0; i < array0.length / 2; i++) {
        int temp = array0[i];
        array0[i] = array0[array0.length - 1 - i];
        array0[array0.length - 1 - i] = temp;
    }
    
    
  }  
  
  
  public static int[] inverter2(int[] array1){
    int[] array5 = copy(array1);
    
    for (int i = 0; i < array5.length / 2; i++) {
        int temp = array5[i];
        array5[i] = array5[array5.length - 1 - i];
        array5[array5.length - 1 - i] = temp;
    }
    
    return array5;
    
}
  
  
  public static void print(int[] list){
    for (int i = 0; i < list.length; i++){
      System.out.print(list[i] + " ");
    }
    System.out.println();
  }
}