/// Olivia Grimes, 2/2/18, cse 2, lab 02
/// The purpose of the program is to create a bicycle cyclometer which will measure speed, distance, etc.The
///
public class Cyclometer {
  // main method required for every Java program
  public static void main(String[] args0) {
    
    // our input data
    
    int secsTrip1=480; // seconds of trip 1
    int secsTrip2=3220; // seconds of trip 2
    int countsTrip1=1561; // rotations for trip 1
    int countsTrip2=9037; // rotations for trip 2
    
    double wheelDiameter=27.0, // wheel diameter
    PI=3.14159, // value of pi
    feetPerMile=5280, // feet in a mile
    inchesPerFoot=12, // inches in a foot
    secondsPerMinute=60; // seconds in a minute
    double distanceTrip1, distanceTrip2, totalDistance; // total disatnces
    
    System.out.println("Trip 1 took " +
                       (secsTrip1/secondsPerMinute)+" minutes and had "+
                       countsTrip1+" counts.");
    System.out.println("Trip 2 took "+
                       (secsTrip2/secondsPerMinute)+" minutes and had "+
                       countsTrip2+" counts.");
    
    // Calculations for Trip 1 and 2 measurements
    
    distanceTrip1=countsTrip1*wheelDiameter*PI;
    // Above gives distance in inches
    distanceTrip1/=inchesPerFoot*feetPerMile; // distance in miles
    distanceTrip2=countsTrip2*wheelDiameter*PI/inchesPerFoot/feetPerMile;
    totalDistance=distanceTrip1+distanceTrip2;
    
    //Print out output data
    System.out.println("Trip 1 was "+distanceTrip1+" miles");
    System.out.println("Trip 2 was "+distanceTrip2+" miles");
    System.out.println("The total distance was "+totalDistance+" miles");
    
  } //end of main method
}
