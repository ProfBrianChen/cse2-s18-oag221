// Olivia Grimes
// hw08 Part 2

import java.util.Scanner; // import scanner

public class RemoveElements{ // class
  public static void main(String [] arg){ // main method
    // declare scanner, arrays and ints
    Scanner scan=new Scanner(System.in);
    int num[]=new int[10];
    int newArray1[];
    int newArray2[];
    int index,target;
    String answer="";
    
    do{
      System.out.print("Random input 10 ints [0-9]");
      num = randomInput();
      String out = "The original array is:";
      out += listArray(num);
      System.out.println(out);
 
      System.out.print("Enter the index ");
      index = scan.nextInt();
      newArray1 = delete(num,index);
      String out1 = "The output array is ";
      out1 += listArray(newArray1); //return a string of the form "{2, 3, -9}"  
      System.out.println(out1);
 
      System.out.print("Enter the target value ");
      target = scan.nextInt();
      newArray2 = remove(num,target);
      String out2 ="The output array is ";
      out2 += listArray(newArray2); //return a string of the form "{2, 3, -9}"  
      System.out.println(out2);
      System.out.print("Go again? Enter 'y' or 'Y', anything else to quit-");
      answer = scan.next();
      System.out.println();
    }while(answer.equals("Y") || answer.equals("y"));
  }
 
  public static String listArray(int num[]){ // list array method
    String out = "{";
    for(int j = 0; j < num.length; j++){
      if(j > 0){
        out += ", ";
      }
      out += num[j];
    }
    out += "} ";
    return out;
  
  }

  public static int[] randomInput(){ // random input method
    int[] array; // declare array
    array = new int[10]; // create space for array
    
    for (int i = 0; i < 10; i++){ // for loop to randomize values of array
      array[i] = (int)(Math.random() * 10);
    }
    return array; // return randomized array
  }
  
  
  public static int[] delete(int[] list, int pos){ // delete method
    int[] array1; // declare array
    array1 = new int[list.length - 1]; // create space for array
    
    if (pos > 9 || pos < 0){ // if position isn't a value of the array, return the original array (list)
      System.out.println("The index is not valid");
      return list;
    }
  
    for (int k = 0; k < list.length - 1; k++){ 
      if (k < pos){
        array1[k] = list[k];
      }
      else if (k >= pos){
        array1[k] = list[k + 1]; // once the position has been met, skips that value
      }
    }
    return array1; // returns new array without the value in position 'pos'
  }
  
  public static int[] remove(int[] list, int target){ // remove method
    int[] array2; // declare new array
    
    int num = 0;
    // for loop that counts number of target values in the array
    for (int t = 0; t < list.length; t++){
      if (list[t] == target){
        num++;
      }
    }
    
    if (num == 0){ // if the target value doesn't appear, return original array (list)
      System.out.println("Element "+ target + " was not found");
      return list;
    }
    
    System.out.println("Element " + target + " was found"); // otherwise, tell user the target value was found
    
    array2 = new int[list.length - num]; // create space for new array
    
    int u = 0;
    // loop that prints a new array without the target value
    for (int s = 0; s < list.length - num; s++){
      if (list[s + u] == target){
        ++u;
        if (list[s + u] == target){
          ++u;
          if (list[s + u] == target){
            ++u;
            if (list[s + u] == target){
              ++u;
            }
          }
        }
      }
      array2[s] = list[s + u];
      
    }
    return array2; // returns new array
    
  }
}