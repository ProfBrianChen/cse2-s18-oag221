// Olivia Grimes
// CSE2 HW8 Part 1

import java.util.Scanner; // import scanner
import java.util.Random; // import random function

public class CSE2Linear{ // class
  public static void main(String[] args){ // main method
    
    int[] finalGrade; // declae array
    finalGrade = new int[15]; // make space for array
    
    Scanner myScanner = new Scanner(System.in); // declare scanner
    System.out.println("Enter 15 ascending ints for final grades in CSE2: "); // prompt user to enter values
    
    for (int i = 0; i < finalGrade.length; i++){ // for loop to check for conditions of inputs -- couldn't figure out how to check for all each time
      while(!myScanner.hasNextInt()){
        String junkWord = myScanner.next();
        System.out.print("Invalid input. Enter an integer: ");
      } finalGrade[i] = myScanner.nextInt();
      while (finalGrade[i] > 100 || finalGrade[i] < 0){
          System.out.print("Not within range. Enter a value between 0 and 100: ");
          finalGrade[i] = myScanner.nextInt();
      }
      if (i > 0){
        while (finalGrade[i] <= finalGrade[i - 1]){
          System.out.print("Invalid input. Enter a value greater than the previous value: ");
          finalGrade[i] = myScanner.nextInt();
        }
      }
    }
    
    
    Scanner scan = new Scanner(System.in); // declare new scanner
    System.out.println("Enter a grade to search for: "); // prompt user to search for a grade
    int grade = scan.nextInt(); // grade to search for
    binarySearch(finalGrade, grade);
    
    randomScrambling(finalGrade, grade); // call method
    
  }
  
  
  // method for linear search
  public static void linearSearch(int[] list, int grades){ 
   
    Scanner scann = new Scanner(System.in); // declare new scanner
    System.out.println("Enter a grade to search for: "); // prompt user to enter a grade to search for
    grades = scann.nextInt();
    
    
    for (int i = 0; i < list.length; i++){
      if (list[i] == grades){
        System.out.println("Found "+ grades);
        return; // search for the grade and return if it is found
      }
    }
    System.out.println("Did not find "+ grades); // prompt it was not found if so
    return;
  }
  
  
  
  // method for binary search
  public static void binarySearch(int[] list, int grade){ 
    
    int first = 0; // declare and initialize variable
    int last = list.length - 1; // declare and initialize variable
    int middle = (first + last) / 2; // declare and initialize variable
    int iteration = 0; // declare and initialize variable
    
    while (first <= last){
      if (list[middle] < grade){
        first = middle + 1; // changes first value if grade being searched for is greater than the middle value
      }
      else if (list[middle] == grade){
        System.out.println(grade +" was found in the list with " + iteration + " iterations");
        return; // returns true if the value is found
      }
      else{
        last = middle - 1; // changes the last value if the grade being searched for is less than the middle value
      }
      iteration++;
      middle = (first + last) / 2; // changes where the middle value is
    }
    System.out.println(grade + " was not found in the list with " + iteration+ " iterations");
    return; // returns false if value is never found
    
    
  }
  
  // method for random scrambling
  public static void randomScrambling(int[] list, int grade){ 
    System.out.println();
    System.out.println("Randomly scrambled numbers: "); // tells user what the next values are
    
    // for loops to randomly scramble and then print the scrambled values
    for(int i = 0; i < list.length; i++){
      int index = (int)(Math.random() * list.length);
      int temp = list[i];
      list[i] = list[index];
      list[index] = temp;
    }
    
    for (int u: list){
      System.out.print(u + " ");
    }
    System.out.println();
    
    linearSearch(list, grade); // call linear search to find a value in the random scramble
  }
  
  
}