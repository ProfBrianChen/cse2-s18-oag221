// Olivia Grimes hw01 2/2/18 cse02
public class WelcomeClass {
  
  public static void main(String[] args) {
    // Prints "----------" to the terminal window
    System.out.println("    -----------");  
    System.out.println("    | WELCOME |");
    System.out.println("    -----------");
    System.out.println("  ^  ^  ^  ^  ^  ^");
    System.out.println(" / \\/ \\/ \\/ \\/ \\/ \\ ");
    System.out.println("<-O--A--G--2--2--1->");
    System.out.println(" \\ /\\ /\\ /\\ /\\ /\\ /");
    System.out.println("  v  v  v  v  v  v ");
    System.out.println("My name is Olivia Grimes. I live in Dravo at Lehigh and am from Boston, MA.");                  
                       
  }

  
}