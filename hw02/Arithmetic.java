// Olivia Grimes, hw 02, 2/3/18 cse02

public class Arithmetic {
  
  public static void main(String[] args) {
    // INPUTS 
    
    // Garment inputs
    int numPants = 3; // number of pairs of pants
    double pantsPrice = 34.98; // price per pair of pantsPrice
    
    int numShirts = 2; // number of pairs of shirts
    double shirtsPrice = 24.99; // price per shirt
    
    int numBelts = 1; // number of belts
    double beltsPrice = 33.99; // price per belt
    
    double paSalesTax = 0.06; // tax rate
    
    // Total price inputs per garment
    double totalCostOfPants = numPants * pantsPrice; // input for total cost of pants
    double totalCostOfShirts = numShirts * shirtsPrice; // input for total cost of shirtsPrice
    double totalCostOfBelts = numBelts * beltsPrice; // input for total cost of belts
    
    // Total tax inputs
    double totalTaxOnPants = totalCostOfPants * paSalesTax; // input for tax on pants
    double totalTaxOnShirts = totalCostOfShirts * paSalesTax; // input for tax on shirts
    double totalTaxOnBelts = totalCostOfBelts * paSalesTax; // input for tax on belts
    
    // Total price of purchases before taxes input
    double totalPriceBeforeTaxes = totalCostOfPants + totalCostOfShirts +
      totalCostOfBelts;
    
    // Total sales tex input
    double totalSalesTax = totalTaxOnPants + totalTaxOnShirts + totalTaxOnBelts;
    
    // Total paid for transaction including sales taxe
    double totalPaidForTransaction = totalPriceBeforeTaxes + totalSalesTax;
    
    
    
    // CALCULATIONS
    
    // Printing total prices of garments
    System.out.println("Total cost of pants is "+
                      totalCostOfPants);
    System.out.println("Total cost of shirts is "+
                      totalCostOfShirts);
    System.out.println("Total cost of belts is "+
                      totalCostOfBelts);
    
    // Sales Tax per garment type
    System.out.println("Sales tax on pants is "+ 
                       String.format("%.2f", totalTaxOnPants)); // tax on pants
    System.out.println("Sales tax on shirts is "+
                       String.format("%.2f", totalTaxOnShirts)); // tax on shirts
    System.out.println("Sales tax on belts is "+
                       String.format("%.2f", totalTaxOnBelts)); // tax on belts
    
    // Total price of purchases before taxes
    System.out.println("Total price of purchases is "+ totalPriceBeforeTaxes);
    
    // Total sales tax
    System.out.println("Total sales tax is "+ String.format("%.2f", totalSalesTax));
    
    // Total paid for transaction, incl sales tax
    System.out.println("The total amount paid for the transaction is "
                       + String.format("%.2f", totalPaidForTransaction));
                      
                       
  }

  
}