// Olivia Grimes
// CSE2 - hw09

import java.util.Random; // import random object

public class DrawPoker{ // class
  public static void main(String[] args){ // main method
    
    
    int[] handOne = new int[5]; // declare hand one
    int[] handTwo = new int[5]; // declare hand two
    
    // scramble and return scrambled array of 52 cards
    int index[] = new int[52];
    for (int i = 0; i < 52; i++){
      index[i] = i;
    }
    int array[] = new int[52];
    array = shuffle(index); 
    
    // dividing the cards to the players
    for (int h = 0; h < 10; h++){
      if (h % 2 == 0){
        handOne[h / 2] = array[h];
      }
      else{
        handTwo[h / 2] = array[h];
      }
      
    }
    
    
    // determining suits of cards for hand one
    String[] suit = new String[5];
    for (int y = 0; y < 5; y++){
      if (handOne[y] / 13 == 0){
        suit[y] = "diamonds";
      }
      else if (handOne[y] / 13 == 1){
        suit[y] = "clubs";
      }
      else if (handOne[y] / 13 == 2){
        suit[y] = "hearts";
      }
      else{
        suit[y] = "spades";
      }
      
    }
    
    // determining suits of cards for hand one
    String[] suit2 = new String[5];
    for (int y = 0; y < 5; y++){
      if (handTwo[y] / 13 == 0){
        suit2[y] = "diamonds";
      }
      else if (handTwo[y] / 13 == 1){
        suit2[y] = "clubs";
      }
      else if (handTwo[y] / 13 == 2){
        suit2[y] = "hearts";
      }
      else{
        suit2[y] = "spades";
      }
    }
    
    
    // determining number value of card
    int number = 0;
    for (int k = 0; k < 5; k++){
    switch(handOne[k] % 13){
      case 0:
        number = 1;
        break;
      case 1:
        number = 2;
        break;
      case 2:
        number = 3;
        break;
      case 3:
        number = 4;
        break;
      case 4:
        number = 5;
        break;
      case 5:
        number = 6;
        break;
      case 6:
        number = 7;
        break;
      case 7:
        number = 8;
        break;
      case 8:
        number = 9;
        break;
      case 9:
        number = 10;
        break;
      case 10:
        number = 11;
        break;
      case 11:
        number = 12;
        break;
      case 12:
        number = 13;
        break;
    }
      handOne[k] = number;
    }
    
    int number2 = 0;
    for (int l = 0; l < 5; l++){
      switch(handTwo[l] % 13){
        case 0:
          number2 = 1;
          break;
        case 1:
          number2 = 2;
          break;
        case 2:
          number2 = 3;
          break;
        case 3:
          number2 = 4;
          break;
      case 4:
        number2 = 5;
        break;
      case 5:
        number2 = 6;
        break;
      case 6:
        number2 = 7;
        break;
      case 7:
        number2 = 8;
        break;
      case 8:
        number2 = 9;
        break;
      case 9:
        number2 = 10;
        break;
      case 10:
        number2 = 11;
        break;
      case 11:
        number2 = 12;
        break;
      case 12:
        number2 = 13;
        break;
    }
      handTwo[l] = number2;
    }
    
    
    // printing hand one
    System.out.println("Hand one: ");
    for (int u = 0; u < 5; u++){
      System.out.print(handTwo[u] + " of " + suit[u] + " ");
    }
    
    System.out.println();
    
    // printing hand 2
    System.out.println("Hand two: ");
    for (int u = 0; u < 5; u++){
      System.out.print(+ handOne[u] + " of "+ suit2[u] + " ");
    } 
    System.out.println();
    
    
    // determine if there is a pair
    if (pair(handOne) == true){
      System.out.println("Hand one has a pair");
    }
    else{
      System.out.println("Hand one does not have a pair");
    }
    
    if (pair2(handTwo) == true){
      System.out.println("Hand two has a pair");
    }
    else{
      System.out.println("Hand two does not have a pair");
    }
    
    
  }
  
  
  // method that shuffles the cards in the deck
  public static int[] shuffle(int[] deck){

    Random ranNum = new Random();
    
    for (int i = 0; i < 52; i++){
      int r = i + ranNum.nextInt(52 - i);
      int temp = deck[r];
      deck[r] = deck[i];
      deck[i] = temp;
    }
    
    
    return deck;
    
    
    
  }
  
  // method that determines if there is a pair for hand one
  public static boolean pair(int[] one){
    
    for (int i = 0; i < 5; i++){
      for (int j = 0; j < 5; j++){
        if (i != j){
          if (one[i] == one[j]){
            return true;
          }
        }
      }
    }
    return false;
  }
  
  
  // method that determines if there is a pair for hand two
  public static boolean pair2(int[] two){
    
    for (int i = 0; i < 5; i++){
      for (int j = 0; j < 5; j++){
        if (i != j){
          if (two[i] == two[j]){
            return true;
          }
        }
      }
    }
    return false;
  }
  
  
  
  
  
  
  
  
  
}