// Olivia Grimes
// 4.6.18 CSE2 Lab 8

import java.util.Scanner;

public class lab08{
  public static void main(String[] args){
    
    // array for number of students
    int numStudents = (int)(Math.random() * 6) + 5;
    String[] students;
    students = new String[numStudents];
    
    // array for midterm grade
    int grade = 0;
    int[] midterm = new int[numStudents];
    
    
    
    // prompt user to enter student names
    System.out.println("Enter "+ numStudents +" students names: ");
    Scanner scan = new Scanner(System.in);
    
    int i = 0;
    
    for (i = 0; i < numStudents; i++){
      students[i] = scan.next();
      grade = (int)(Math.random() * 100);
      midterm[i] = grade;
    }
    
    System.out.println();
    System.out.println("Here are the midterm grades of the "+ numStudents +" students above:");
    
    for (i = 0; i < numStudents; i++){
      System.out.println(students[i] + ": " + midterm[i]);
    }

    
    
  }
}