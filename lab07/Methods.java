// Olivia Grimes, Lab 7

import java.util.Random; // import random generator

public class Methods{ // class
  // main method
  public static void main(String[] args){
    
    String act = "adg";
    action(act); // call method containing action sentences and called intro sentence
    
    String xxx = "asd";
    conclusion(xxx); // call method containing conclusion sentence

  }
  
  
  // method containing adjectives
  public static String adjectives(String adj){
    Random randomGenerator = new Random();
    int randomInt = randomGenerator.nextInt(10); // randomly generate a number 0-9
    
    
    switch (randomInt){
      case 0:
        adj = "ugly";
        break;
      case 1:
        adj = "pretty";
        break;
      case 2:
        adj = "funny";
        break;
      case 3:
        adj = "clean";
        break;
      case 4:
        adj = "fresh";
        break;
      case 5:
        adj = "happy";
        break;
      case 6:
        adj = "silly";
        break;
      case 7:
        adj = "sweet";
        break;
      case 8:
        adj = "sour";
        break;
      case 9:
        adj = "sticky";
        break;     
    }
    
    return adj;
  }
  
  
  // method containing subject nouns
  public static String subjectNouns(String subN){
    Random randomGenerator = new Random();
    int randomInt = randomGenerator.nextInt(10); // randomly generate a number 0-9
    
    // subN = "asd";
    
    switch (randomInt){
      case 0:
        subN = "employee";
        break;
      case 1:
        subN = "dog";
        break;
      case 2:
        subN = "cat";
        break;
      case 3:
        subN = "president";
        break;
      case 4:
        subN = "computer";
        break;
      case 5:
        subN = "brother";
        break;
      case 6:
        subN = "girls";
        break;
      case 7:
        subN = "phone";
        break;
      case 8:
        subN = "shark";
        break;
      case 9:
        subN = "money";
        break;     
    }
    
    return subN;
  } 
  
  
  // method containing past verbs
  public static String pastVerbs(String pastV){
    Random randomGenerator = new Random();
    int randomInt = randomGenerator.nextInt(10); // randomly generate a number 0-9
    
    // pastV = "afo";
    
    switch (randomInt){
      case 0:
        pastV = "sang";
      case 1:
        pastV = "threw";
        break;
      case 2:
        pastV = "drank";
        break;
      case 3:
        pastV = "thought";
        break;
      case 4:
        pastV = "played";
        break;
      case 5:
        pastV = "ran";
        break;
      case 6:
        pastV = "painted";
        break;
      case 7:
        pastV = "broke";
        break;
      case 8:
        pastV = "walked";
        break;
      case 9:
        pastV = "poked";
        break;     
    }
    return pastV;
  }
  
  
  // method containing object nouns
  public static String objectNouns(String objN){
    Random randomGenerator = new Random();
    int randomInt = randomGenerator.nextInt(10); // randomly generate a number 0-9
    
    // objN = "afj";
    
    switch (randomInt){
      case 0:
        objN = "stapler";
        break;
      case 1:
        objN = "bike";
        break;
      case 2:
        objN = "plant";
        break;
      case 3:
        objN = "book";
        break;
      case 4:
        objN = "money";
        break;
      case 5:
        objN = "drawing";
        break;
      case 6:
        objN = "mouse";
        break;
      case 7:
        objN = "brush";
        break;
      case 8:
        objN = "bracelet";
        break;
      case 9:
        objN = "diamond";
        break;     
    }
    return objN;
  }
  
  // method containing thesis statement
  public static String thesis(String sub){
    String adj = "abc";
    String objN = "dfg";
    String pastV = "hij";
    String subN = "tgb";
    sub = subjectNouns(subN); // declaring subject so it remains constant throughout the program
    
    System.out.println("The "+ adjectives(adj) + " "+ adjectives(adj) + " "+ sub + " " + pastVerbs(pastV) + " the " + adjectives(adj) + " "+ objectNouns(objN) + ".");
    return sub;
  }
  
  // method containing action sentences
  public static String action(String act){
    Random randomGenerator2 = new Random();
    int randomInt2 = randomGenerator2.nextInt(6) + 4; // randomly generate a number 4-9
    
    String adj = "abc";
    String objN = "dfg";
    String pastV = "hij";
    String subN = "tgb";
    String sub = "tgb";
    String subject = thesis(sub);
    int i = 1;
    
    while (i < randomInt2 / 2){
      System.out.println("Then the "+ subject + " " + pastVerbs(pastV) + " and " +  pastVerbs(pastV) + " the "+ objectNouns(objN) + ".");
      System.out.println("It "+ pastVerbs(pastV) + " the "+ objectNouns(objN) + " and "+ pastVerbs(pastV) + " " + objectNouns(objN) + " at the "+ adjectives(adj) + " "+ objectNouns(objN) + ".");
      i++;
    }
    
    return subject;
    
  }
  
  // method containing conclusion
  public static void conclusion(String xxx){
    String adj = "abc";
    String objN = "dfg";
    String pastV = "hij";
    String subN = "tgb";
    String subject = "tgb";
    xxx = action(subject);
    
    System.out.println("That " + xxx + " " + pastVerbs(pastV) + " their " + objectNouns(objN) + "!");
  }
  
}