// Olivia Grimes
// hw10 CSE2 4/21/18

import java.util.Random;

public class RobotCity{
  public static void main(String[] args){
    // printing a heading and spacing it out nicely
    System.out.println();
    System.out.println("City:");
    System.out.println();
    
    // code creating the city and calling the method buildCity to do so
    int[][] newCity;
    newCity = buildCity();
    display(newCity);
    
    // printing a heading and spacing it out nicely
    System.out.println();
    System.out.println("Invaded city:");
    System.out.println();
    
    // code to invade the city by calling the method that creates spaces for invading robots
    int k = (int)(Math.random() * 5) + 6;
    int[][] newArray = invade(newCity, k);
    display(newArray);
    
    // printing a heading and spacing it out nicely
    System.out.println();
    System.out.println("Updated city:");
    System.out.println();
    
    // code to update the invaded city by calling the method that moves the robots
    for (int i = 0; i < 5; i++){
      int[][] newArray2 = update(newArray);
      display(newArray2);
      System.out.println();
    }
    
    
  }
  
  // method that creates the city
  public static int[][] buildCity(){
    int rows = (int)(Math.random() * 6) + 10; // number of rows in array
    int columns = (int)(Math.random() * 6) + 10; // number of columns in city
    
    int[][] array = new int[rows][columns]; // declaring and creating space for a new array
    
    for (int i = 0; i < rows; i++){
      for (int j = 0; j < columns; j++){
        array[i][j] = (int)(Math.random() * 900) + 100; // randomized population in each city block
      }
    }
    return array; // returns new array containing the city
    
  }
  
  // method that prints the city array
  public static void display(int[][] array){
    
    for (int i = 0; i < array.length; i++){
      for (int j = 0; j < array[i].length; j++){
        System.out.printf("%5d", array[i][j]); // prints array put into the method
      }
      System.out.println();
    }
    
  }
  
  // method that invades the city with robots
  public static int[][] invade(int[][] array, int k){
    
    int rows = array.length; // number of rows
    int columns = array[0].length; // number of columns
    int[][] checkArray = new int[rows][columns];
    
    
    for (int i = 0; i < k; i++){
      int randRow = (int)(Math.random() * rows); // choosing a random column for robot to land
      int randColumn = (int)(Math.random() * columns); // choosing a random row for robot to land
      
      while (checkArray[randRow][randColumn] == 1){ // check that a robot is not already occupying the space
        randRow = (int)(Math.random() * rows); // choosing a random column for robot to land
        randColumn = (int)(Math.random() * columns); // choosing a random row for robot to land
      }
      checkArray[randRow][randColumn] = 1; // set check array equal to 1 at randomized position, used to check the next loop around that a robot is not already occupying the spot
      
      int val = array[randRow][randColumn]; // setting the value of the array at the given coordinates equal to an int
      val = -val; // turning the int negative to show a robot has landed here
      array[randRow][randColumn] = val; // reassigning the coordinates of the array to the negative value of before to indicate a robot has landed
    }
    return array; // returning the array with the invaded city
    
  }
  
  // method to update the city once the robots have moved east
  public static int[][] update(int[][] array){
    
    int rows = array.length; // number of rows in city array
    int columns = array[0].length; // number of columns in city array
    
    for (int i = 0; i < rows; i++){ // rows
      for (int j = columns - 1; j >= 0; j--){ // columns
        int val = array[i][j]; // setting an integer value equal to the value at the position
        if (val < 0){ // if the value is neg, aka contains a robot, resets it to a pos number
          val = -val;
          array[i][j] = val;
          if (j < columns - 1){ // moves robot eastward
            int val1 = array[i][j + 1];
            val1 = -val1;
            array[i][j + 1] = val1;
          }
        }
      }
    }
    return array; // return array with robots moved eastward
    
  }
  
  
}