// Olivia Grimes
// hw06 - Argyle

import java.util.Scanner; // import scanner

public class Argyle{ // class
  public static void main (String[] args){ // main method
    
    Scanner myScanner = new Scanner(System.in);
    
    
    // INPUTS
    
    // allows user to enter a width of viewing window
    int widthViewing = 1;
    
    do {System.out.print("Enter a positive integer for the width of viewing window: ");
      while (!myScanner.hasNextInt()){
        String junkWord = myScanner.next();
        System.out.print("Enter a positive integer for the width of viewing window: ");
      }
      widthViewing = myScanner.nextInt();
     } while (widthViewing <= 0);
    
    
    // allows user to enter a height of viewing window
    int heightViewing = 1;
    
    do {System.out.print("Enter a positive integer for the height of viewing window: ");
        while (!myScanner.hasNextInt()){
          String junkWord = myScanner.next();
          System.out.print("Enter a positive integer for the height of viewing window: ");
        }
        heightViewing = myScanner.nextInt();
    } while (heightViewing <= 0);
    
    // allows user to enter a width of argyle diamonds
    int widthDiamonds = 1;
    
    do {System.out.print("Enter a positive integer for the width of the argyle diamonds: ");
        while (!myScanner.hasNextInt()){
          String junkWord = myScanner.next();
          System.out.print("Enter a positive integer for the width of the argyle diamonds: ");
        }
        widthDiamonds = myScanner.nextInt();
    } while (widthDiamonds <= 0);
    
    // allows user to enter a width of argyle center stripe
    int widthCenterStripe = 1;
    
    do {System.out.print("Enter a positive odd integer, no larger than half of the diamond size, for the width of the argyle center stripe: ");
        while (!myScanner.hasNextInt()){
          String junkWord = myScanner.next();
          System.out.print("Enter a positive odd integer, no larger than half of the diamond size, for the width of the argyle center stripe: ");
         }
         widthCenterStripe = myScanner.nextInt();
    } while (widthCenterStripe <= 0 || widthCenterStripe % 2 == 0 || widthCenterStripe > (widthDiamonds / 2));
    
    // allows user to enter first character for pattern fill
    System.out.print("Enter a first character for the pattern fill: ");
    String char1 = myScanner.next();
    char result1 = char1.charAt(0);
    
    // allows user to enter a second character for pattern fill
    System.out.print("Enter a second character for the pattern fill: ");
    String char2 = myScanner.next();
    char result2 = char2.charAt(0);
    
    // allows user to enter a third character for stripe fill
    System.out.print("Enter a third character for the stripe fill: ");
    String stripeFill = myScanner.next();
    char result3 = stripeFill.charAt(0);
    
    
    
    // PROGRAM After Inputs
    
    for (int i = 1; i < heightViewing + 1; i++){ // determining viewing height for loop
        for (int j = 1; j < widthViewing + 1; j++){ // determining viewing width for loop
          int q = (j - 1) / widthDiamonds; // declaring a variable q as an integer
          if ((q + 3) % 2 == 1){ // creating pattern
                // prints stripes
                if ((((widthDiamonds - (j % widthDiamonds) + i + 2) % widthDiamonds) <= (widthCenterStripe)) && (((widthDiamonds - (j % widthDiamonds) + i + 2) % widthDiamonds) > 0)){
                  System.out.print(result3);
                }
                // prints part of diamond
                else if (((j + widthDiamonds) % widthDiamonds) <= (widthDiamonds - (i % widthDiamonds)) && (j % widthDiamonds != 0)){
                  System.out.print(result1);
                }
                // prints other symbols in diamond
                else{
                  System.out.print(result2);
                }
              
            
          }
          else if ((q + 3) % 2 == 0){ // second condition set for second pattern
                // prints stripes
                if ((((widthDiamonds + (j % widthDiamonds) + i + 2) % widthDiamonds) <= (widthCenterStripe)) && (((widthDiamonds + (j % widthDiamonds) + i + 2) % widthDiamonds) > 0)){
                  System.out.print(result3);
                }
                // prints part of diamond
                else if (((j + widthDiamonds) % widthDiamonds) <= i % widthDiamonds && (j % widthDiamonds != 0)){
                  System.out.print(result2);
                }
                // prints other symbols in diamond
                else{
                  System.out.print(result1);
                }
          }
        }
        System.out.println(); // creates new line
      }
    
    
    
    
  }
}